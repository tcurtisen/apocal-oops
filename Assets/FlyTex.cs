﻿using OpenGET;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FlyTex : MonoBehaviour
{

    public Fader f;
    public CanvasGroup cg;
    public TextMeshProUGUI text;

    public void Awake() {
        f = new Fader(cg);
        startPos = transform.position;
        f.FadeOut();
        f.OnFadeComplete += complete;
    }

    private Vector2 startPos = new Vector2(0, 0);
    private float timing = 0;

    public void Update() {
        timing += Time.deltaTime;
        transform.position = Vector2.Lerp(startPos, startPos + new Vector2(0, 300.0f), Mathf.Clamp01(timing));
    }

    private void complete(Fader fade, int dir) {
        if (dir < 0) {
            Destroy(this.gameObject);
        }
    }

}
