﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoDuration : MonoBehaviour
{
    UnityEngine.Video.VideoPlayer vp;

    void Start()
    {
        vp = gameObject.GetComponent<UnityEngine.Video.VideoPlayer>();
        StartCoroutine(WaitForEnd());
    }

    IEnumerator WaitForEnd()
    {
        double duration = vp.clip.length;
        yield return new WaitForSeconds((float)duration);
        SceneManager.LoadScene("Tutorial"); 
    }
}
