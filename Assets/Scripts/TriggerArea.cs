﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerArea : MonoBehaviour
{

    public UnityEvent OnTriggerStay;

    private void Awake()
    {
        Debug.Assert(OnTriggerStay != null);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            OnTriggerStay.Invoke();
        }
    }

}
