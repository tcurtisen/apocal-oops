﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    public Inventory.LimbType piece;
    public Inventory.PieceTypes pieceType;
    public Sprite sprite;

    public bool canPickup = false;

    private void Awake()
    {
        sprite = gameObject.GetComponentInParent<SpriteRenderer>().sprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            canPickup = true;
            collision.GetComponent<CollisionCheck>().touchingItem = this;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            canPickup = false;
            collision.GetComponent<CollisionCheck>().touchingItem = null;
        }
    }



}
