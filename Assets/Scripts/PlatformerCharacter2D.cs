using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
        [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
        [SerializeField] private bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;            // Whether or not the player is grounded.
        private Transform m_CeilingCheck;   // A position marking where to check for ceilings
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
        [SerializeField]
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.

        private const int DEFAULT_FLAPS = 3;
        [HideInInspector]
        public int flapCount = DEFAULT_FLAPS;
        private bool flapRechargeRunning = false;
        public GameObject buttonPrompt;

        public bool frozen = false;

        PlayerSounds playerSounds;
        public AudioSource audioSource;

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
            m_CeilingCheck = transform.Find("CeilingCheck");
            Debug.Assert(m_Anim != null);
            m_Rigidbody2D = GetComponent<Rigidbody2D>();

            Time.timeScale = 1.0f;

            playerSounds = GetComponent<PlayerSounds>();
            //audioSource = GetComponent<AudioSource>();
        }

        private void FixedUpdate()
        {
            //Check for player being grounded for resetting flap count
            if (m_Grounded && !flapRechargeRunning && flapCount != DEFAULT_FLAPS)
            {
                StartCoroutine(flapRecharge());
            }
            if (m_Grounded) {
                ProgressStepCycle(m_FacingRight ? -m_Rigidbody2D.velocity.x : m_Rigidbody2D.velocity.x);
            }

            bool wasGrounded = m_Grounded;
            m_Grounded = false;

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                    m_Grounded = true;
            }
            m_Anim.SetBool("Ground", m_Grounded);

            /*if (!wasGrounded && m_Grounded) {
                audioSource.PlayOneShot(playerSounds.landingSounds[Random.Range(0, playerSounds.landingSounds.Length)]);
            }*/


            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
        }


        public void Move(float move, bool crouch, bool jump)
        {
            // If crouching, check to see if the character can stand up
            if (!crouch && m_Anim.GetBool("Crouch"))
            {
                // If the character has a ceiling preventing them from standing up, keep them crouching
                if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
                {
                    crouch = true;
                }
            }

            // Set whether or not the character is crouching in the animator
            m_Anim.SetBool("Crouch", crouch);

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // Reduce the speed if crouching by the crouchSpeed multiplier
                move = (crouch ? move*m_CrouchSpeed : move);

                // The Speed animator parameter is set to the absolute value of the horizontal input.
                m_Anim.SetFloat("Speed", Mathf.Abs(move));

                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move*m_MaxSpeed, m_Rigidbody2D.velocity.y);

                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
                    // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
            }
            // If the player should jump...
            if (m_Grounded && jump && m_Anim.GetBool("Ground"))
            {
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Anim.SetBool("Ground", false);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));

                audioSource.PlayOneShot(playerSounds.jumpSounds[0]);
            }
            //if jumping in air and can flap, then flap
            else if(!m_Grounded && jump && flapCount > 0)
            {
                Debug.Log(flapCount);
                audioSource.PlayOneShot(playerSounds.jumpSounds[flapCount]);
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
                flapCount--;
            }
        }

        private float m_StepCycle;
        private float m_NextStep;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;

        private void ProgressStepCycle(float speed) {
            if (m_Rigidbody2D.velocity.sqrMagnitude > 0) {
                m_StepCycle += (m_Rigidbody2D.velocity.magnitude + (speed * m_RunstepLenghten)) *
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep)) {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }

        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioSource footstepSource;

        private void PlayFootStepAudio() {
            if (!m_Grounded) {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, playerSounds.stepSounds.Length);
            footstepSource.clip = playerSounds.stepSounds[n];
            footstepSource.PlayOneShot(footstepSource.clip);
            // move picked sound to index 0 so it's not picked next time
            playerSounds.stepSounds[n] = playerSounds.stepSounds[0];
            playerSounds.stepSounds[0] = footstepSource.clip;
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            Vector3 promptScale = buttonPrompt.transform.localScale;
            theScale.x *= -1;
            promptScale.x *= -1;
            transform.localScale = theScale;
            buttonPrompt.transform.localScale = promptScale;
            
        }


        IEnumerator flapRecharge()
        {
            flapRechargeRunning = true;

            if(m_Grounded)
            {
                bool finished = false;
                for (int i = 0; i < 5; i++)
                {
                    yield return new WaitForSeconds(0.5f);
                    if(!m_Grounded)
                    {
                        finished = true;
                        break;
                    }
                }
                if (!finished)
                {
                    flapCount = DEFAULT_FLAPS;
                }
            }

            flapRechargeRunning = false;
        }
    }
}
