﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Image headImage, rightArmImage, leftArmImage, rightLegImage, leftLegImage;
    public Sprite heads, rightarms, leftarms, rightlegs, leftlegs;
    public enum PieceTypes { HUMAN, WOLF, CAT, NONE};
    public enum LimbType { HEAD, LEFTARM, RIGHTARM, LEFTLEG, RIGHTLEG };

    public GameObject prefabPiece;

    public PieceTypes head = PieceTypes.NONE;
    public PieceTypes leftArm = PieceTypes.NONE;
    public PieceTypes rightArm = PieceTypes.NONE;
    public PieceTypes leftLeg = PieceTypes.NONE;
    public PieceTypes rightLeg = PieceTypes.NONE;

    public int size = 0;

    private void Awake() {
        heads = headImage.sprite;
        leftarms = leftArmImage.sprite;
        rightarms = rightArmImage.sprite;
        leftlegs = leftLegImage.sprite;
        rightlegs = rightLegImage.sprite;
    }

    public void Clear() {
        head = PieceTypes.NONE;
        leftArm = PieceTypes.NONE;
        rightArm = PieceTypes.NONE;
        leftLeg = PieceTypes.NONE;
        rightLeg = PieceTypes.NONE;
        headImage.sprite = heads;
        rightArmImage.sprite = rightarms;
        leftArmImage.sprite = leftarms;
        rightLegImage.sprite = rightlegs;
        leftLegImage.sprite = leftlegs;
    }

    public void SwapBodyParts(LimbType limb, PieceTypes pieceType, Sprite pieceImage)
    {
        switch(limb)
        {
            case LimbType.HEAD:
                if (head != PieceTypes.NONE)
                {
                    DropPiece(head, limb, pieceImage);
                }
                else
                {
                    size++;
                }
                head = pieceType;
                headImage.sprite = pieceImage;
                break;
            case LimbType.LEFTARM:
                if(leftArm != PieceTypes.NONE)
                {
                    DropPiece(leftArm, limb, pieceImage);
                }
                else
                {
                    size++;
                }
                leftArm = pieceType;
                leftArmImage.sprite = pieceImage;
                break;
            case LimbType.RIGHTARM:
                if (rightArm != PieceTypes.NONE)
                {
                    DropPiece(rightArm, limb, pieceImage);
                }
                else
                {
                    size++;
                }
                rightArm = pieceType;
                rightArmImage.sprite = pieceImage;
                break;
            case LimbType.LEFTLEG:
                if (leftLeg != PieceTypes.NONE)
                {
                    DropPiece(leftLeg, limb, pieceImage);
                }
                else
                {
                    size++;
                }
                leftLeg = pieceType;
                leftLegImage.sprite = pieceImage;
                break;
            case LimbType.RIGHTLEG:
                if (rightLeg != PieceTypes.NONE)
                {
                    DropPiece(rightLeg, limb, pieceImage);
                }
                else
                {
                    size++;
                }
                rightLeg = pieceType;
                rightLegImage.sprite = pieceImage;
                break;
            default:
                break;
        }
    }

    void DropPiece(PieceTypes pieceType, LimbType piece, Sprite pieceImage)
    {
        //replace game object with prefab
        GameObject droppedPiece = GameObject.Instantiate(prefabPiece, gameObject.transform.position, Quaternion.identity);
        droppedPiece.transform.GetChild(0).GetComponent<BodyPart>().pieceType = pieceType;
        droppedPiece.transform.GetComponent<SpriteRenderer>().sprite = pieceImage;
    }
}
