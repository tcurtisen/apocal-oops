﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    public AudioClip[] jumpSounds;
    public AudioClip[] landingSounds;
    public AudioClip[] stepSounds;

}
