﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{

    public TextMeshProUGUI timerText;

    public float timeLimitSeconds = 120.0f;

    private float timeRemaining;

    private Action onComplete = null;

    private void Awake() {
        Debug.Assert(timerText != null);
    }

    public void Countdown(Action onTimerEnd) {
        onComplete = onTimerEnd;
        timeRemaining = timeLimitSeconds;
    }

    public float GetRemainingTime()
    {
        return timeRemaining;
    }

    void Update()
    {
        TimeSpan timeNow = TimeSpan.FromSeconds(timeRemaining);
        timerText.text = string.Format("{0:D2}:{1:D2}", timeNow.Minutes, timeNow.Seconds);
        if (onComplete != null) {
            timeRemaining -= Time.deltaTime;
            if (timeRemaining < 0.0f) {
                onComplete();
                onComplete = null;
            }
        }
    }
}
