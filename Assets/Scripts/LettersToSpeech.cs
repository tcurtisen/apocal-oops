﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LettersToSpeech : MonoBehaviour
{
    public AudioClip[] letters;
    public AudioSource audioSource;


    public void PlayMessage(string message)
    {
       if(!audioSource.isPlaying)
        {

            StartCoroutine(PlayLetter(message));
        }
    }

    IEnumerator PlayLetter(string message)
    {
        foreach (char c in message.ToCharArray())
        {
            if (char.IsLetter(c))
            {
                AudioClip clipToPlay = letters[(int)char.ToUpper(c) - 64 - 1];
                audioSource.clip = clipToPlay;
                audioSource.Play();
                yield return new WaitForSeconds(clipToPlay.length / 4);
            }
        }
    }

}
