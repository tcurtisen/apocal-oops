﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    public GameController gameController;
    public int score = 0;

    private const int HUMAN_SCORE = 200;
    private const int WOLF_SCORE = 100;

    public float GetScore(Inventory.PieceTypes piece) {
        switch (piece) {
            case Inventory.PieceTypes.HUMAN:
                return HUMAN_SCORE;
            case Inventory.PieceTypes.WOLF:
                return WOLF_SCORE;
            default:
                return HUMAN_SCORE;
        }
    }

    public void CalculateScore(Inventory.PieceTypes head, Inventory.PieceTypes leftArm,
                        Inventory.PieceTypes rightArm, Inventory.PieceTypes torso,
                        Inventory.PieceTypes leftLeg, Inventory.PieceTypes rightLeg)
    {
        int scoreToAdd = 0;

        List<Inventory.PieceTypes> pieces = new List<Inventory.PieceTypes>();
        pieces.Add(head);
        pieces.Add(leftArm);
        pieces.Add(rightArm);
        pieces.Add(torso);
        pieces.Add(leftLeg);
        pieces.Add(rightLeg);

        foreach(Inventory.PieceTypes aPiece in pieces)
        {
            switch (aPiece)
            {
                case Inventory.PieceTypes.HUMAN:
                    scoreToAdd += HUMAN_SCORE;
                    break;
                case Inventory.PieceTypes.WOLF:
                    scoreToAdd += WOLF_SCORE;
                    break;
            }
        }

        bool isOneType = true;
        for(int i = 0; i < pieces.Count; i++)
        {
            if(pieces[0] != pieces[i])
            {
                isOneType = false;
            }
        }

        if(isOneType)
        {
            scoreToAdd *= 2;
        }

        score += scoreToAdd;
    }

    public void TimeBonus()
    {
        score += Mathf.RoundToInt(gameController.timer.GetRemainingTime());
    }

   
}
