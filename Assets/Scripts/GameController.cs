﻿using OpenGET;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class GameController : MonoBehaviour
{

    public SpeechController speechBubble;
    public SpeechController bigBubble;
    public GameTimer timer;
    public JoinBodies bodyJoinerPrefab;
    public Camera2DFollow cam;
    public GameObject player;
    public ScoreSystem scoring;
    public FlyTex scoreText;
    public Transform scoreSpawn;
    public Inventory inv;
    public TextMeshProUGUI UIScoreText;

    public int bodiesRequired = 1;

    public void ShowFlyingText(string message) {
        FlyTex spawned = Instantiate<FlyTex>(scoreText, scoreSpawn);
        spawned.text.text = message;
    }

    public void AddScore(int amount) {
        scoring.score += amount;
        UIScoreText.text = string.Format("{0}", scoring.score);
    }

    private Fader endFade;
    public CanvasGroup endFadeBlack;

    private void EndGame(Fader fff, int dir) {
        // Load menu?
        Coroutines.StopAll();
        SceneManager.LoadScene("Menu");
    }

    public AudioClip holySound;

    private void DelayEndGame(Fader fff, int dir) {
        fff.FadeIn(4.0f);
        fff.OnFadeComplete -= DelayEndGame;
        fff.OnFadeComplete += EndGame;
    }

    public AudioSource otherSource;

    public void AddBody() {
        bodiesRequired--;
        if (bodiesRequired <= 0) {
            otherSource.PlayOneShot(holySound);
            // End game
            bigBubble.Show("GOD", speechBubble.godImage, string.Format("Well done Angel BOII, you have put together all bodies in the time limit! I give you a blessing score of {0}", scoring.score));
            player.GetComponent<PlatformerCharacter2D>().frozen = true;
            if (endFade == null) {
                endFade = new Fader(endFadeBlack);
                endFadeBlack.alpha = 0.01f;
                endFade.FadeOut(4.0f);
                endFade.OnFadeComplete += DelayEndGame;
            }
        }
    }

    private void Awake() {
        Debug.Assert(speechBubble != null);
        Debug.Assert(timer != null);
    }

    private void Start() {
        timer.Countdown(OnTimerEnd);
    }

    private void OnTimerEnd() {
        //   speechBubble.Show("Time's up!");
        // End game
        bigBubble.Show("GOD", speechBubble.godImage, "Dang it! You're outta time Angel BOII...");
        player.GetComponent<PlatformerCharacter2D>().frozen = true;
        if (endFade == null) {
            endFade = new Fader(endFadeBlack);
            endFade.FadeIn(4.0f);
            endFade.OnFadeComplete += EndGame;
        }
        Debug.Log("Game over!");
    }

}
