﻿using OpenGET;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;
using Random = UnityEngine.Random;

public class JoinBodies : MonoBehaviour
{
    public GameObject torso;
    public GameObject arm_left;
    public GameObject arm_right;
    public GameObject leg_left;
    public GameObject leg_right;
    public GameObject head;

    public Transform midLeft;
    public Transform midRight;
    public Transform bottomLeft;
    public Transform bottomRight;
    public Transform top;

    public GameController game;

    private Fader fade;

    private float time = 0.0f;

    private Vector2 startPos = new Vector2();

    private void Awake() {
        startPos = transform.position;
    }

    public void DoIt(Sprite btorso, Sprite barm_left, Sprite barm_right, Sprite bleg_left, Sprite bleg_right, Sprite bhead) {
        torso.GetComponent<SpriteRenderer>().sprite = btorso;
        arm_left.GetComponent<SpriteRenderer>().sprite = barm_left;
        arm_right.GetComponent<SpriteRenderer>().sprite = barm_right;
        leg_left.GetComponent<SpriteRenderer>().sprite = bleg_left;
        leg_right.GetComponent<SpriteRenderer>().sprite = bleg_right;
        head.GetComponent<SpriteRenderer>().sprite = bhead;
        time = 0.0f;
    }

    private Dictionary<string, bool> map = new Dictionary<string, bool>();
    public AudioSource asource;
    public AudioClip[] squishies = null;

    void TriggerOnce(string id, Action trigger) {
        if (!map.ContainsKey(id)) {
            map.Add(id, true);
            trigger();
            Log.Debug("TRIGGERING WITH ID " + id);
        }
    }

    private void OnDestroy() {
        game.AddBody();
    }

    private void Update() {
        if (time >= 0.0f) {
            time += Time.deltaTime;
            game.player.GetComponent<PlatformerCharacter2D>().frozen = true;
            if (time <= 3.0f) {
                torso.transform.position = Vector2.Lerp(torso.transform.position, transform.position, time / 3.0f);
                Vector2 oldPos = transform.position;
            } else if (time <= 4.0f) {
                arm_left.transform.position = Vector2.Lerp(arm_left.transform.position, midLeft.position, time - 3.0f);
                TriggerOnce("Slam0", () => {
                    asource.PlayOneShot(squishies[Random.Range(0, squishies.Length)]);
                    float score = game.scoring.GetScore(game.inv.leftArm);
                    game.ShowFlyingText(string.Format("+{0}", score));
                    game.AddScore((int)score);
                });
            } else if (time <= 5.0f) {
                TriggerOnce("slam1", () => { 
                    asource.PlayOneShot(squishies[Random.Range(0, squishies.Length)]);
                    float score = game.scoring.GetScore(game.inv.rightArm);
                    game.ShowFlyingText(string.Format("+{0}", score));
                    game.AddScore((int)score);
                });
                arm_right.transform.position = Vector2.Lerp(arm_right.transform.position, midRight.position, time - 4.0f);
            } else if (time <= 6.0f) {
                TriggerOnce("slam2", () => {
                    asource.PlayOneShot(squishies[Random.Range(0, squishies.Length)]);
                    float score = game.scoring.GetScore(game.inv.leftLeg);
                    game.ShowFlyingText(string.Format("+{0}", score));
                    game.AddScore((int)score);
                });
                leg_left.transform.position = Vector2.Lerp(leg_left.transform.position, bottomLeft.position, time - 5.0f);
            } else if (time <= 7.0f) {
                TriggerOnce("slam3", () => {
                    asource.PlayOneShot(squishies[Random.Range(0, squishies.Length)]);
                    float score = game.scoring.GetScore(game.inv.rightLeg);
                    game.ShowFlyingText(string.Format("+{0}", score));
                    game.AddScore((int)score);
                });
                leg_right.transform.position = Vector2.Lerp(leg_right.transform.position, bottomRight.position, time - 6.0f);
            } else if (time <= 8.0f) {
                TriggerOnce("slam4", () => {
                    asource.PlayOneShot(squishies[Random.Range(0, squishies.Length)]);
                    float score = game.scoring.GetScore(game.inv.head);
                    game.ShowFlyingText(string.Format("+{0}", score));
                    game.AddScore((int)score);
                });
                head.transform.position = Vector2.Lerp(head.transform.position, top.position, time - 7.0f);
            } else if (time <= 10.0f) {
                TriggerOnce("slam4", () => {
                    asource.PlayOneShot(squishies[Random.Range(0, squishies.Length)]);
                });
            } else if (time <= 12.0f) {
                game.cam.target = game.player.transform;
                transform.position = Vector2.Lerp(transform.position, startPos + new Vector2(0, 6.0f), (time - 10.0f) / 2.0f);
            }
            else {
                game.cam.target = game.player.transform;
                game.player.GetComponent<PlatformerCharacter2D>().frozen = false;
                time = -1;
                Destroy(gameObject);
            }

        }

    }

}
