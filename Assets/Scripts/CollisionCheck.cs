﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionCheck : MonoBehaviour
{
    public GameObject buttonPrompt;
    public GameController game;

    AudioSource audioSource;
    PlayerSounds playerSounds;
    Inventory inventory;

    public BodyPart touchingItem;
    public LettersToSpeech lettersToSpeech;

    private void Awake() {
        Debug.Assert(game != null);
        audioSource = GetComponent<AudioSource>();
        playerSounds = GetComponent<PlayerSounds>();
        inventory = GetComponent<Inventory>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && touchingItem != null)
        {
            //Debug.Log("hello trigger");
            BodyPart bp = touchingItem;
            if (inventory.size >= 5 && bp.tag == "Torso") {
                inventory.size = 0;
                // Body join!
                GameObject body = GameObject.Instantiate(game.bodyJoinerPrefab.gameObject, new Vector2(touchingItem.transform.position.x, touchingItem.transform.position.y + 6.0f), Quaternion.identity);
                JoinBodies joiner = body.GetComponent<JoinBodies>();
                joiner.game = game;
                joiner.DoIt(
                    touchingItem.transform.parent.gameObject.GetComponent<SpriteRenderer>().sprite,
                    inventory.leftArmImage.sprite,
                    inventory.rightArmImage.sprite,
                    inventory.leftLegImage.sprite,
                    inventory.rightLegImage.sprite,
                    inventory.headImage.sprite
                );
                game.cam.target = body.transform;
                Destroy(touchingItem.gameObject.transform.parent.gameObject);
                game.speechBubble.Hide();
                game.inv.Clear();
            } else if (bp.tag != "Torso") {
                inventory.SwapBodyParts(bp.piece, bp.pieceType, bp.sprite);
                Destroy(touchingItem.gameObject.transform.parent.gameObject);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Interactable")
        {
            game.speechBubble.Show("GOD", game.speechBubble.godImage, "Aha, you found a body part!", 0.3f);
            lettersToSpeech.PlayMessage("Aha, you fou!");
            buttonPrompt.SetActive(true);
            //set button prompt
        }
        if (col.gameObject.tag == "Torso") {
            if (inventory.size >= 5) {
                game.speechBubble.Show("GOD", game.speechBubble.godImage, "Excellent, you have all the body parts! Press E to join them...", 0.3f);
                lettersToSpeech.PlayMessage("Excellent, you hav");
                buttonPrompt.SetActive(true);
            } else {
                game.speechBubble.Show("GOD", game.speechBubble.godImage, "Come back when you have more body parts!", 0.3f);
                lettersToSpeech.PlayMessage("Come back whe");
            }
        }

        if(col.gameObject.tag == "test")
        {
            game.speechBubble.Show("Angel", game.speechBubble.angelImage, "Oh God, they're all dead...", 0.3f);
            lettersToSpeech.PlayMessage("Oh God, they're all dead...");
        }

        if(col.gameObject.tag == "proxMessage")
        {
            ProxMessage pm = col.gameObject.GetComponent<ProxMessage>();
            game.speechBubble.Show("GOD", game.speechBubble.godImage, pm.message, 0.3f);
            lettersToSpeech.PlayMessage(pm.message.Substring(0, pm.message.Length / 2));
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Interactable" || col.gameObject.tag == "test")
        {
            game.speechBubble.Hide(0.3f);
            buttonPrompt.SetActive(false);
        }
    }
}
