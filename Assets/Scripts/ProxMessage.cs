﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProxMessage : MonoBehaviour
{
    public string message;
    public GameController game;
    public LettersToSpeech lettersToSpeech;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            game.speechBubble.Show("GOD", game.speechBubble.godImage, message, 0.3f);

            lettersToSpeech.PlayMessage(message.Substring(0, message.Length / 2));
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            game.speechBubble.Hide(0.3f);
        }
    }
}
