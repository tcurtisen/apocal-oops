﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OpenGET;
using TMPro;

public class SpeechController : MonoBehaviour
{

    public CanvasGroup bubble;
    public TextMeshProUGUI speechText;
    public TextMeshProUGUI chatName;
    public Image chatHead;

    public Sprite godImage;
    public Sprite angelImage;


    public bool isFadingIn;

    private Fader doFade;

    private void Awake() {
        Debug.Assert(bubble != null);
        bubble.alpha = 0;
        doFade = new Fader(bubble);
        doFade.OnFadeComplete += OnFadeComplete;
    }

    private void OnFadeComplete(Fader f, int fadeDir) {
        if (fadeDir < 0) {
            bubble.gameObject.SetActive(false);
        }
    }

    public void Show(string chatName, Sprite chatHead, string message, float fadeTime = 1.0f)
    {
        this.chatHead.sprite = chatHead;
        this.chatName.text = chatName;
    
        speechText.text = message;
        bubble.gameObject.SetActive(true);
        if (!doFade.isFullyVisible) {
            doFade.FadeIn(fadeTime);
        }
    }

    public void Hide(float fadeTime = 1.0f)
    {
        if (doFade != null && !doFade.isFullyHidden) {
            doFade.FadeOut(fadeTime);
        }
    }

}
